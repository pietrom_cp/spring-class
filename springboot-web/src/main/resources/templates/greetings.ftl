<#import "/spring.ftl" as spring/>
<!DOCTYPE HTML>
<html>
<head>
<title><@spring.message "hello.title" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	<p>
		Hello <b>${remoteUser}</b>
	</p>
	<p>${message}</p>
	<p>
		<strong>Available roles:</strong>
		<ul>
		    <#list roles as auth>
		        <li>${auth.authority}</li>
		    </#list>
		</ul>
	</p>
	<p>
		<a href="${contextPath}/secrets">Read <em>secrets</em>!</a>
	</p>

	<form class="form-inline" action="<@spring.url "/custom-logout-url" />" method="post">
		<input type="submit" value="Log out" />
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</body>
</html>