package com.codiceplastico.learning.spring.web.healthcheck;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/health-check")
public class HealthCheckController {
    @RequestMapping("/echo")
    public String hello(Model model, @RequestParam(value="text", required=false, defaultValue="Hello, World!") String text) {
        model.addAttribute("text", text);
        return "echo";
    }
}

