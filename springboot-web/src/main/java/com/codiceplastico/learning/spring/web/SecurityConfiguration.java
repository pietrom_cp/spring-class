package com.codiceplastico.learning.spring.web;

import com.codiceplastico.learning.spring.web.security.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .addFilterAfter(preAuthFilter(), RequestHeaderAuthenticationFilter.class)
            .authorizeRequests()
                .antMatchers("/secrets/**").hasAnyRole("ADMIN")
                .antMatchers("/greetings/**").hasAnyRole("USER,PREAUTH_USER")
            .and()
                .formLogin()
                //.loginPage("/login-form")
                .loginProcessingUrl("/custom-login-url")
                .defaultSuccessUrl("/greetings/greet?target=Spring World")
            .and()
                .logout()
                .logoutUrl("/custom-logout-url")
            .and()
                .authenticationProvider(basicAuthenticationProvider());

    }

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth, AuthenticationProvider provider) throws Exception {
//        auth.authenticationProvider(provider);
//
////        auth
////            .inMemoryAuthentication()
////            .withUser("user").password("password").roles("USER")
////            .and()
////            .withUser("admin").password("password").roles("USER", "ADMIN");
//    }

    @Bean
    public PasswordEncoder encoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    public CustomAuthenticationProvider basicAuthenticationProvider() {
        Collection<InMemoryUser> users = new LinkedList<>();
        users.add(new InMemoryUser("user", "password", "abc", Arrays.asList("ROLE_USER")));
        users.add(new InMemoryUser("admin", "password", "def", Arrays.asList("ROLE_USER", "ROLE_ADMIN")));
        users.add(new InMemoryUser("pietro", "password", "ghi", Arrays.asList("ROLE_USER", "ROLE_ADMIN")));
        return new CustomAuthenticationProvider(users);
    }

    public CustomPreAuthAuthenticationProvider preAuthAuthenticationProvider() {
        return new CustomPreAuthAuthenticationProvider();
    }

    public CustomPreAuthFilter preAuthFilter() {
        CustomPreAuthFilter filter = new CustomPreAuthFilter();
        filter.setAuthenticationManager(authManager());
        filter.setAuthenticationDetailsSource(new CustomPreAuthDetailsSource());
        return filter;
    }

    private AuthenticationManager authManager() {
        return new ProviderManager(Arrays.asList(basicAuthenticationProvider(), preAuthAuthenticationProvider()));
    }
}
