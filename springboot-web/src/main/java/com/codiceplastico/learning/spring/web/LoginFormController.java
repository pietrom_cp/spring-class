package com.codiceplastico.learning.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("login-form")
public class LoginFormController {
    @RequestMapping(value="", method = RequestMethod.GET)
    public ModelAndView loginForm(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("login-form");
        mav.addObject("contextPath", request.getContextPath());
        return mav;
    }
}
