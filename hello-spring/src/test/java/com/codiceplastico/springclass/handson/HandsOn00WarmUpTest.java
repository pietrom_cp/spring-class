package com.codiceplastico.springclass.handson;

import org.junit.Test;

import java.util.Random;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class HandsOn00WarmUpTest {
    @Test
    public void canIncrementCounter() {
        Counter counter = new Counter();
        int count = new Random().nextInt(100);
        for (int i = 0; i < count; i++) {
            counter.increment();
        }
        //assertThat(counter.getValue(), is(count));
    }
}
