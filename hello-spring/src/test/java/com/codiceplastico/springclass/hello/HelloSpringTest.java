package com.codiceplastico.springclass.hello;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringTest {
	@Test
	public void initializeBeanWithStringConstructorArg() throws Exception {
		try (AbstractApplicationContext context = new ClassPathXmlApplicationContext("hello-world-context.xml")) {
			Hello component = context.getBean(Hello.class);
			Assert.assertNotNull(component);
			Assert.assertEquals("Hi, World!", component.sayHello());
		}
	}

	@Test
	public void initializeBeanWithRefConstructorArg() throws Exception {
		try (AbstractApplicationContext context = new ClassPathXmlApplicationContext("hello-world-context.xml")) {
			HelloFromSource component = context.getBean(HelloFromSource.class);
			Assert.assertNotNull(component);
			Assert.assertEquals("Hola, World!", component.sayHello());
		}

	}

	@Test
	public void initializeBeanWithInlineConstructorArg() throws Exception {
		try (AbstractApplicationContext context = new ClassPathXmlApplicationContext(
				"hello-world-inline-context.xml")) {
			HelloFromSource component = context.getBean(HelloFromSource.class);
			Assert.assertNotNull(component);
			Assert.assertEquals("Hiii, World!", component.sayHello());
		}
	}
}
