package com.codiceplastico.springclass.handson;

public class Bar {
    private final Counter counter;

    public Bar(Counter counter) {
        this.counter = counter;
    }

    public void execute() {
       counter.increment();
    }
}
