package com.codiceplastico.springclass.hello;

public class Hello {
	private final String message;
	
	public Hello(String message) {
		this.message = message;
	}

	public Hello() {
		this("Hello");
	}
	
	public String sayHello() {
		return String.format("%s, World!", this.message);
	} 
}