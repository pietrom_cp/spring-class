package com.codiceplastico.springclass.hello;

public interface MessageSource {
	public String getMessage();
}
