package com.codiceplastico.springclass.hello;

import java.util.Collection;
import java.util.stream.Collectors;

public class GreetingsHandler {
	private final Collection<Hello> greetings;

	public GreetingsHandler(Collection<Hello> greetings) {
		this.greetings = greetings;
	}
	
	public int greetingsCount() {
		return this.greetings.size();
	}
	
	public Collection<String> sayHello() {
		return greetings.stream().map(x -> x.sayHello()).collect(Collectors.toList());
	}
}
