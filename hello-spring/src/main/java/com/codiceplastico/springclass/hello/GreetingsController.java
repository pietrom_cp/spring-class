package com.codiceplastico.springclass.hello;

import java.util.stream.Collectors;

public class GreetingsController {
	private final GreetingsHandler handler;
	private final String delimiter;
	
	public GreetingsController(GreetingsHandler handler, String delimiter) {
		this.handler = handler;
		this.delimiter = delimiter;
	}
	
	public String greetings() {
		return handler.sayHello().stream().collect(Collectors.joining(delimiter)); 
	}
}
