package com.codiceplastico.springclass.hello;

public class FixedMessageSource implements MessageSource {
	private final String message;
	
	public FixedMessageSource(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
