package com.codiceplastico.springclass.hello.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ColorController {
	@Autowired
	private Red red;
	@Autowired
	private Blue blue;
	@Autowired
	private Green green;
	
	public Red red() {
		return red;
	}
	
	public Blue blue() {
		return blue;
	}
	
	public Green green() {
		return green;
	}
}
