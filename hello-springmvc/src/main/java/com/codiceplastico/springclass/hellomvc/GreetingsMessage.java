package com.codiceplastico.springclass.hellomvc;

public class GreetingsMessage {
    private final String message;

    public GreetingsMessage(String message) {
        this.message = message;
    }
    
    public String getMessage() {
		return message;
	}
}
