package com.codiceplastico.springclass.hellomvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codiceplastico.springclass.hello.HelloFromSource;

@RestController
@RequestMapping("hello")
public class HelloController {
    private final HelloFromSource helloService;
    @Autowired
    public HelloController(HelloFromSource helloService) {
        this.helloService = helloService;
    }

    @RequestMapping(value = "/{target}", method = RequestMethod.GET)
    public GreetingsMessage getMessageFor(@PathVariable(value="target") String target) {
        final String message = helloService.sayHelloTo(target);
        return new GreetingsMessage(message);
    }
}
