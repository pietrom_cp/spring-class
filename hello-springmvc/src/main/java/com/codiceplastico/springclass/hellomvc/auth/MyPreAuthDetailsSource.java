package com.codiceplastico.springclass.hellomvc.auth;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AuthenticationDetailsSource;

public class MyPreAuthDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, MyUserDetails> {
	@Override
	public MyUserDetails buildDetails(HttpServletRequest request) {
		String username = request.getHeader("__the_username");
		String company = request.getHeader("__the_company");
		MyUserDetails details = new MyUserDetails(username, company);
		return details;
	}
}
