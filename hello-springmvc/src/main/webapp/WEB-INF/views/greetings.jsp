<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<title><spring:message code="hello.title" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	<p>
		Hello <b><c:out value="${pageContext.request.remoteUser}" /></b>
	</p>
	<p>${message}</p>
	<p>
		<strong>Available roles:</strong>
		<ul>
			<c:forEach var="auth" items="${roles}">
				<li><c:out value="${auth.authority}" /></li>
			</c:forEach>
		</ul>
	</p>
	<p>
		<strong>User info:</strong>
		CompanyId: <c:out value="${userInfo.companyId}" />
	</p>
	<p>
		<a href="${pageContext.request.contextPath}/api/secrets">Read <em>secrets</em>!</a>
	</p>
	<c:url var="logoutUrl" value="/logout" />
	<form class="form-inline" action="${logoutUrl}" method="post">
		<input type="submit" value="Log out" /> <input type="hidden"
			name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</body>
</html>