package com.codiceplastico.springclass.batch;

import org.springframework.batch.core.launch.support.CommandLineJobRunner;

public class Hello00Main {

	public static void main(String[] args) throws Exception {
		CommandLineJobRunner.main(new String[] {"hello00-job.xml", "simpleJob"});
	}

}
