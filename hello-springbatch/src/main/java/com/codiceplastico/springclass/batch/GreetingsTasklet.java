package com.codiceplastico.springclass.batch;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class GreetingsTasklet implements Tasklet {
	private final String message;
	
	public GreetingsTasklet(String message) {
		this.message = message;
	}
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		System.out.println(String.format("%s, Batch World!", message));
		boolean ok = true;
		if(ok) {
			return RepeatStatus.FINISHED;
		}
		throw new RuntimeException("Error in step!");
	}

}
